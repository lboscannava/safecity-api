from django.conf.urls import url, include
from django.contrib import admin

from api.views import IndexView

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', IndexView.as_view(), name='index'),
    url(r'^api/', include('api.urls'))
]
