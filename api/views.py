import base64
import os
from django.core.files import File
from django.http import HttpResponse
from django.views.generic import View
from rest_framework import serializers
from rest_framework import status
from rest_framework import viewsets
from rest_framework.response import Response

from api.utils import send_push_notification
from .models import SafeCityPhoto, AndroidDevice, PushNotification, PushNotificationEnum, QualityEnum
from .serializers import (
    SafeCityPhotoSerializer,
    AndroidDeviceSerializer,
    PushNotificationSerializer
)


class SafeCityPhotoViewSet(viewsets.ModelViewSet):
    queryset = SafeCityPhoto.objects.all()
    serializer_class = SafeCityPhotoSerializer

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        message = 'Photo posted successfully'

        device_id = serializer.validated_data['device_id']
        encode_image = serializer.validated_data['encode_image']
        filename = serializer.validated_data['name']

        if 'quality' in serializer.validated_data:
            quality = serializer.validated_data['quality']
        else:
            quality = QualityEnum.low.value

        if SafeCityPhoto.objects.filter(device_id=device_id, name=filename, quality=quality):
            return Response({'info': message})

        try:
            image_data = base64.b64decode(encode_image)
            with open(filename, 'wb') as image:
                image.write(image_data)
        except Exception:
            raise serializers.ValidationError("Error decoding image")

        data = serializer.validated_data
        del data['encode_image']
        image = File(open(filename, 'rb'))
        data['image'] = image

        SafeCityPhoto.objects.create(**data)

        os.remove(filename)

        return Response({'info': message})


class AndroidDeviceViewSet(viewsets.ModelViewSet):
    queryset = AndroidDevice.objects.all()
    serializer_class = AndroidDeviceSerializer


class PushNotificationViewSet(viewsets.ModelViewSet):
    queryset = PushNotification.objects.all()
    serializer_class = PushNotificationSerializer

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        option = serializer.data['options']
        data = None
        device_id = None

        if option == PushNotificationEnum.settings.value:
            if 'settings' not in serializer.data:
                return Response(
                    {'error': 'Error you need to upload a settings file'},
                    status=status.HTTP_400_BAD_REQUEST
                )
            settings = serializer.data['settings']
            data = dict(settings=settings)
        elif option == PushNotificationEnum.updates.value:
            if 'apk' not in serializer.data:
                return Response(
                    {'error': 'Error you need to upload an apk file'},
                    status=status.HTTP_400_BAD_REQUEST
                )
            apk = serializer.data['apk']
            data = dict(apk=apk)
        elif option == PushNotificationEnum.hd_photo.value:
            if serializer.data['photo_name'] == "":
                return Response(
                    {'error': 'Error you need to put the name of the photo'},
                    status=status.HTTP_400_BAD_REQUEST
                )
            photo_name = serializer.data['photo_name']
            if SafeCityPhoto.objects.filter(name=photo_name):
                device_id = SafeCityPhoto.objects.filter(name=photo_name).last().device_id
            data = dict(photo_name=photo_name)

        send_push_notification(data, device_id=device_id)

        PushNotification.objects.create(**serializer.data)

        return Response(serializer.data)


class IndexView(View):
    def get(self, request, *args, **kwargs):
        return HttpResponse('SafeCity API')
