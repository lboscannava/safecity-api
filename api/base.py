from enum import Enum


class ChoicesEnum(Enum):
    @classmethod
    def choices(cls):
        return [
            (m[1].value, m[0])
            for m in cls.__members__.items()
            ]
