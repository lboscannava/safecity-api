from rest_framework import serializers

from .models import SafeCityPhoto, AndroidDevice, PushNotification


class SafeCityPhotoSerializer(serializers.ModelSerializer):
    encode_image = serializers.CharField(write_only=True)

    class Meta:
        model = SafeCityPhoto
        fields = ('device_id', 'name', 'camera', 'quality', 'encode_image', 'image')
        extra_kwargs = {
            'image': {'read_only': True}
        }


class AndroidDeviceSerializer(serializers.ModelSerializer):
    class Meta:
        model = AndroidDevice
        fields = ('device_id', 'token',)


class PushNotificationSerializer(serializers.ModelSerializer):
    class Meta:
        model = PushNotification
        fields = ('options', 'photo_name', 'settings', 'apk', 'created_on',)
