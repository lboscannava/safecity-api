from django.db import models

from api.base import ChoicesEnum


class QualityEnum(ChoicesEnum):
    low = "Low"
    high = "High"


class CameraIDEnum(ChoicesEnum):
    one = "1"
    two = "2"


class PushNotificationEnum(ChoicesEnum):
    settings = 1
    updates = 2
    hd_photo = 3


class SafeCityPhoto(models.Model):
    device_id = models.CharField(max_length=300)
    name = models.CharField(max_length=200)
    camera = models.CharField(
        max_length=2,
        choices=CameraIDEnum.choices(),
        default=CameraIDEnum.one.value
    )
    quality = models.CharField(
        max_length=10,
        choices=QualityEnum.choices(),
        default=QualityEnum.low.value
    )
    image = models.ImageField()

    def __str__(self):
        return '%s' % self.name

    def save(self, *args, **kwargs):
        if not SafeCityPhoto.objects.filter(
                device_id=self.device_id,
                quality=self.quality,
                name=self.name
        ):
            super(SafeCityPhoto, self).save(*args, **kwargs)


class AndroidDevice(models.Model):
    device_id = models.CharField(max_length=300)
    token = models.CharField(max_length=300)

    def __str__(self):
        return self.device_id

    def save(self, *args, **kwargs):
        if not AndroidDevice.objects.filter(device_id=self.device_id):
            super(AndroidDevice, self).save(*args, **kwargs)


class PushNotification(models.Model):
    options = models.SmallIntegerField(
        choices=PushNotificationEnum.choices(),
        default=PushNotificationEnum.hd_photo.value
    )
    photo_name = models.CharField(max_length=300, blank=True)
    settings = models.FileField(blank=True)
    apk = models.FileField(blank=True)
    created_on = models.DateField(auto_now_add=True)

    def __str__(self):
        return "{} - {}".format(self.options, self.created_on)
