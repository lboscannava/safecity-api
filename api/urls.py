from django.conf.urls import include, url
from rest_framework import routers

from api import views

router = routers.DefaultRouter()
router.register(r'scphotos', views.SafeCityPhotoViewSet)
router.register(r'android-devices', views.AndroidDeviceViewSet)
router.register(r'push-notifications', views.PushNotificationViewSet)

urlpatterns = [
    url(r'^', include(router.urls)),
    url(r'^api-auth/', include(
        'rest_framework.urls',
        namespace='rest_framework')
        )
]
