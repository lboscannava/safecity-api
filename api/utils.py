from gcm import GCM

from api.models import AndroidDevice
from safecityapi.settings import API_KEY


def send_push_notification(data, device_id=None):
    gcm = GCM(API_KEY)
    response = None
    if device_id:
        android_device = AndroidDevice.objects.filter(device_id=device_id).last()
        if android_device:
            response = gcm.json_request(
                registration_ids=[android_device.token],
                data=data
            )
    else:
        response = gcm.json_request(
            registration_ids=get_token_list(),
            data=data
        )

    return response


def get_token_list():
    tokens = []
    android_devices = AndroidDevice.objects.all()

    for android_device in android_devices:
        tokens.append(android_device.token)

    return tokens
