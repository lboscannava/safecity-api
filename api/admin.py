from django.contrib import admin

from .models import SafeCityPhoto, AndroidDevice, PushNotification

admin.site.register(SafeCityPhoto)
admin.site.register(AndroidDevice)
admin.site.register(PushNotification)
